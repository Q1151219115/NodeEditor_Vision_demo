﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ST.Library.UI.NodeEditor;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using HalconDotNet;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using 视觉框架测试.Tool;
using static 视觉框架测试.Form3;

namespace 视觉框架测试.ImgSource
{
    [STNode("图像采集", "LQ", "2020/12/29", "V1.0", "Image Node")]
    public class ImageInputNode : ImageBaseNode
    {
        HObject Himage = new HObject();
        Tool_LocalImg Tool_LocalImg = new Tool_LocalImg();
        private string _FileName;//默认的DescriptorType不支持文件路径的选择 所以需要扩展
        [STNodeProperty("InputImage", "Click to select a image")]
        public string FileName {
            get { return _FileName; }
            set {
                Image img = null;                       //当文件名被设置时 加载图片并 向输出节点输出
                if (!string.IsNullOrEmpty(value))
                {
                    img = Image.FromFile(value);
                }
                if (m_img_draw != null) m_img_draw.Dispose();
                m_img_draw = img;
                _FileName = value;
                Himage.Dispose();
                HOperatorSet.ReadImage(out Himage,value);
                Tool_LocalImg.Path = value;
                m_op_img_out.TransferData(new BaseTool {Image = Himage }, true);
                this.Invalidate();
            }
        }

        protected override void OnCreate() {
            base.OnCreate();
            this.Title = "导入本地图像";
            HandledSetimg handledimg = new HandledSetimg(Delegate_img);
            Form3.handledSetimg = handledimg;
            m_op_img_out.Connecting += M_op_img_out_Connecting;
            m_op_img_out.DisConnected += M_op_img_out_DisConnected;
            Tool_LocalImg.IsFist = true;
            Tool_LocalImg.STN_guid = this.Guid;
        }

        private void M_op_img_out_DisConnected(object sender, STNodeOptionEventArgs e)
        {
            Tool_LocalImg.OutputNode_guid.Remove(e.TargetOption.Owner.Guid);
        }

        private void M_op_img_out_Connecting(object sender, STNodeOptionEventArgs e)
        {
            Tool_LocalImg.OutputNode_guid.Add(e.TargetOption.Owner.Guid);
        }

        private void Delegate_img(HObject himage)
        {
            if (himage.IsInitialized())
            {
                m_op_img_out.TransferData(new BaseTool { Image = himage }, true);

            }
        }
        public BaseTool Get_Tool()
        {
            return Tool_LocalImg;
        }
        protected override void OnDrawBody(DrawingTools dt) {
            base.OnDrawBody(dt);
            Graphics g = dt.Graphics;
            Rectangle rect = new Rectangle(this.Left + 10, this.Top + 30, 140, 80);
            g.FillRectangle(Brushes.Gray, rect);
            if (m_img_draw != null) g.DrawImage(m_img_draw, rect);
        }
    }
}
