﻿using ChoiceTech.Halcon.Control;
using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewWindow.Config;
using ViewWindow.Model;
using 视觉框架测试.STNodeModel;
using 视觉框架测试.Tool;

namespace 视觉框架测试
{
    public partial class Form2 : Form
    {
        public delegate void HandledSetVal(Tool_DataCode tool_QRCode);
        HandledSetVal handledSetVal;
        Tool_DataCode tool_DataCode;
        static Form2 frm2;
/*        public Form2()
        {
            InitializeComponent();
        }*/

        public Form2(Tool_DataCode tool_dataCode, HandledSetVal handledSetVal)
        {
            InitializeComponent();
            this.handledSetVal = handledSetVal;
            tool_DataCode = tool_dataCode;
            //DisplayVal(tool_QRCode);
            //this.SetVal(ref this.tool_QR1);
        }
        private void DisplayVal(Tool_DataCode tool_QRCode)
        {
            if (tool_QRCode.Search_Region.Lenth1 == 0 && tool_QRCode.Search_Region.Lenth2 == 0)
            {
                //hWindow_Final1.viewWindow.removeActiveROI(ref this.regions);
                hWindow_Final1.viewWindow.genRect2(200.0, 200.0, 0 / 180.0 * Math.PI, 200.0, 300.0, ref this.regions);
                //设置roi的颜色
                this.regions.Last().Color = "blue";
            }
            else
            {

                Rectangle2 Rect2 = tool_QRCode.Search_Region;
                hWindow_Final1.viewWindow.genRect2(Rect2.Row, Rect2.Column, Rect2.Phi, Rect2.Lenth1, Rect2.Lenth2, ref regions);

                hWindow_Final1.viewWindow.displayROI(regions);
                hWindow_Final1.viewWindow.selectROI(0);
            }
            cbxType.Text = tool_QRCode.CodeType;
            numericUpDown1.Text = tool_QRCode.ResultMaxNum.ToString();

        }
        List<ROI> regions = new List<ROI>();//roi集合
        private void SetVal(ref Tool_DataCode tool_DataCode)
        {
            if (regions.Count > 0)
            {
                ROIRectangle2 roirec2 = regions[0] as ROIRectangle2;
                Rectangle2 rectangle2 = new Rectangle2(roirec2.Row, roirec2.Column, roirec2.Phi, roirec2.Lenth1, roirec2.Lenth2);
                tool_DataCode.Search_Region = rectangle2;
            }
            tool_DataCode.ResultMaxNum = int.Parse(numericUpDown1.Text);
            tool_DataCode.TimerOut = int.Parse(nudTimerOut.Text);
            tool_DataCode.CodeType = cbxType.Text;

        }
        /// <summary>
        /// 单实例
        /// </summary>
        public static Form2 SingleShow(Tool_DataCode tool_DataCode, HandledSetVal handledSetVal)
        {
            if (frm2 == null)
            {
                frm2 = new Form2(tool_DataCode, handledSetVal);
            }
            return frm2;
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            if (tool_DataCode != null)
            {
                HOperatorSet.GetImageSize(tool_DataCode.Image,out HTuple width,out HTuple height);
                HOperatorSet.SetPart(hWindow_Final1.hWindowControl.HalconWindow,0,0,height,width);
                hWindow_Final1.hWindowControl.MouseUp += HWindowControl_MouseUP;
                hWindow_Final1.HobjectToHimage(tool_DataCode.Image);
                //tool_QR1.Image.DispObj(hWindow_Final1.hWindowControl.HalconWindow);
/*                if (tool_DataCode.Search_Region != null)
                {
                    Rectangle2 Rect2 = tool_DataCode.Search_Region;
                    hWindow_Final1.viewWindow.genRect2(Rect2.Row, Rect2.Column, Rect2.Phi, Rect2.Lenth1, Rect2.Lenth2, ref regions);
                }*/
                if (tool_DataCode.Result == null)
                {
                    tool_DataCode.Result = String.Empty;
                }
            }
        }
        private int ROI_index;
        List<HTuple> selROI_data;
        private void HWindowControl_MouseUP(object sender, MouseEventArgs e)
        {
            ViewWindow.Model.ROI roi = hWindow_Final1.viewWindow.smallestActiveROI(out selROI_data, out ROI_index);//可以获取最后一次选中或者编辑对象的数据
            if (regions.Count > ROI_index && ROI_index >= 0)
            {
                regions[ROI_index] = roi;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

            hWindow_Final1.HobjectToHimage(tool_DataCode.Image);
            hWindow_Final1.viewWindow.displayROI(this.regions);
            this.SetVal(ref this.tool_DataCode);
            if (tool_DataCode.Search_Region != null)
            {
                HOperatorSet.GenRectangle2(out HObject rect2, tool_DataCode.Search_Region.Row, tool_DataCode.Search_Region.Column, tool_DataCode.Search_Region.Phi, tool_DataCode.Search_Region.Lenth1, tool_DataCode.Search_Region.Lenth2);
                HOperatorSet.AreaCenter(rect2, out HTuple area,out HTuple center_row,out HTuple center_column);
                if (tool_DataCode.is_Follow)
                {
                    if (tool_DataCode.Row != 0)
                    {
                        HOperatorSet.GenCrossContourXld(out HObject cross, tool_DataCode.Row, tool_DataCode.Column, 100, tool_DataCode.Angle);
                        hWindow_Final1.DispObj(cross);
                        tool_DataCode.Row_Dvalue = tool_DataCode.Row - center_row;
                        tool_DataCode.Column_Dvalue = tool_DataCode.Column - center_column;
                    }
                }
            }
            BaseTool baseTool = tool_DataCode.Run(this.tool_DataCode);
            tool_DataCode.Def_Value = baseTool.Result;
            handledSetVal(this.tool_DataCode);
            hWindow_Final1.DispText(baseTool.Result, "yellow", 300, 50, 20, "mono", "true", "true");
            hWindow_Final1.DispObj(baseTool.SymbolXLDs);
            label1.Text = baseTool.ConsumeTime.ToString() + "ms"; 

        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            frm2 = null;
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            DisplayVal(tool_DataCode);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                tool_DataCode.is_Follow = true;
            }
            else
            {
                tool_DataCode.is_Follow = false;
            }
        }
    }
}
