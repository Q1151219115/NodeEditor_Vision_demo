﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewWindow.Config;
using ViewWindow.Model;
using 视觉框架测试.Tool;

namespace 视觉框架测试
{
    public partial class ShapeForm : Form
    {
        public delegate void HandledSetVal_shape(Tool_ShapeMatching tool_ShapeMatching);
        Tool_ShapeMatching Tool_ShapeMatch;
        HandledSetVal_shape handledSetVal;
        static ShapeForm Shapeform;
        public ShapeForm(Tool_ShapeMatching tool_Shape, HandledSetVal_shape handledSetVal)
        {
            InitializeComponent();
            this.handledSetVal = handledSetVal;
            Tool_ShapeMatch = tool_Shape;
        }
        List<ViewWindow.Model.ROI> regions;//roi集合
        private void ShapeForm_Load(object sender, EventArgs e)
        {

            if (Tool_ShapeMatch != null)
            {
                HOperatorSet.GetImageSize(Tool_ShapeMatch.Image, out HTuple width, out HTuple height);
                HOperatorSet.SetPart(hWindow_Final1.hWindowControl.HalconWindow, 0, 0, height, width);
                hWindow_Final1.hWindowControl.MouseUp += HWindowControl_MouseUP;
                hWindow_Final1.HobjectToHimage(Tool_ShapeMatch.Image);
                if (Tool_ShapeMatch.Result == null)
                {
                    Tool_ShapeMatch.Result = String.Empty;
                }
            }

        }
        private int ROI_index;
        List<HTuple> selROI_data;
        private void HWindowControl_MouseUP(object sender, MouseEventArgs e)
        {
            ViewWindow.Model.ROI roi = hWindow_Final1.viewWindow.smallestActiveROI(out selROI_data, out ROI_index);//可以获取最后一次选中或者编辑对象的数据
            if (regions.Count > ROI_index && ROI_index >= 0)
            {
                regions[ROI_index] = roi;
            }
        }

        /// <summary>
        /// 单实例
        /// </summary>
        public static ShapeForm SingleShow(Tool_ShapeMatching tool_Shape, HandledSetVal_shape handledSetVal)
        {
            if (Shapeform == null)
            {
                Shapeform = new ShapeForm(tool_Shape, handledSetVal);
            }
            return Shapeform;
        }

        private void ShapeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Shapeform = null;
        }

        private void ShapeForm_Shown(object sender, EventArgs e)
        {
            DisplayVal(Tool_ShapeMatch);
        }

        private void DisplayVal(Tool_ShapeMatching Tool_ShapeMatch)
        {
            if (Tool_ShapeMatch.Search_Region.Lenth1 == 0 && Tool_ShapeMatch.Search_Region.Lenth2 == 0)
            {
                hWindow_Final1.viewWindow.genRect2(200.0, 200.0, 0 / 180.0 * Math.PI, 200.0, 300.0, ref this.regions);
                //设置roi的颜色
                this.regions.Last().Color = "blue";
            }
            else
            {
                Rectangle2 Rect2 = Tool_ShapeMatch.Search_Region;
                hWindow_Final1.viewWindow.genRect2(Rect2.Row, Rect2.Column, Rect2.Phi, Rect2.Lenth1, Rect2.Lenth2, ref regions);
                hWindow_Final1.viewWindow.displayROI(regions);
                hWindow_Final1.viewWindow.selectROI(0);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hWindow_Final1.HobjectToHimage(Tool_ShapeMatch.Image);
            hWindow_Final1.viewWindow.displayROI(this.regions);
            this.SetVal(ref this.Tool_ShapeMatch);
            Tool_ShapeMatch.Create_shapeModel();
            BaseTool baseTool = Tool_ShapeMatch.Run(this.Tool_ShapeMatch);
            if (baseTool.Score > 0.5)
            {
                /*                HOperatorSet.GenCrossContourXld(out HObject cross, baseTool.Row, baseTool.Column,100, baseTool.Angle);
                                hWindow_Final1.DispObj(cross);*/
                double score = baseTool.Score * 100;
                string str = "Row:" + Math.Round(baseTool.Row,0).ToString() + "\r\n" + "Column:" + Math.Round(baseTool.Column, 0).ToString() + "\r\n" + "Aangle:" + Math.Round(baseTool.Angle, 2).ToString() + "\r\n" + Math.Round(score, 0).ToString() + "%";
                hWindow_Final1.DispText(str, "yellow", 300, 50, 20, "mono", "true", "true");
                //hWindow_Final1.DispObj(Tool_ShapeMatch.SymbolXLDs[i]);

            }
            handledSetVal(Tool_ShapeMatch);

            label1.Text = baseTool.ConsumeTime.ToString() + "ms";
        }

        private void SetVal(ref Tool_ShapeMatching tool_ShapeMatch)
        {
            if (regions.Count > 0)
            {
                ROIRectangle2 roirec2 = regions[0] as ROIRectangle2;
                Rectangle2 rectangle2 = new Rectangle2(roirec2.Row, roirec2.Column, roirec2.Phi, roirec2.Lenth1, roirec2.Lenth2);
                Tool_ShapeMatch.Search_Region = rectangle2;
            }
        }
    }
}
