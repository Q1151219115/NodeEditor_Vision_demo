﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewWindow.Config;
using 视觉框架测试.Tool;

namespace 视觉框架测试
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Form1 setform1;
        public static Dictionary<Guid,BaseTool> Guid_tool = new Dictionary<Guid, BaseTool>();
        private void button1_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();  
            form3.Show(); 
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            setform1 = this;
            var SavePath = Application.StartupPath + @"\配置信息\";
            try
            {
                string path = SavePath + "11.tool";
                if (File.Exists(path))
                {
                    using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        if (fs.Length != 0)
                        {
                            BinaryFormatter bf = new BinaryFormatter();
                            Guid_tool = bf.Deserialize(fs) as Dictionary<Guid, BaseTool>;
                        }
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        int Num = 1;
        private void button2_Click(object sender, EventArgs e)
        {
            //Task.Run(implement_flow);
            Task task = new Task(implement_flow);
            task.Start();
        }


        public async void implement_flow()
        {
            Stopwatch allTool_time = new Stopwatch();
            allTool_time.Start();
            BaseTool tool = new BaseTool();
            Dictionary<Guid, BaseTool> Result_dic = new Dictionary<Guid, BaseTool>();
            Dictionary<Guid, BaseTool> all_Result_dic = new Dictionary<Guid, BaseTool>();
            List<BaseTool[]> Result_list = new List<BaseTool[]>();
            tool = Guid_tool.Values.Single(fisttool => fisttool.IsFist == true);
            tool = tool.Run();
            Result_dic.Add(tool.STN_guid, tool);
            bool is_last = false;
            int i = 1;
            while (!is_last)
            {
                if (Result_dic.Values.Count > 0)
                {
                    foreach (BaseTool basetool in Result_dic.Values)
                    {
                        List<Task<BaseTool>> list_task = new List<Task<BaseTool>>();
                        Stopwatch sw1 = new Stopwatch();
                        sw1.Start();
                        tool = basetool;
                        foreach (Guid node_guid in basetool.OutputNode_guid)
                        {
                            list_task.Add(Task.Run(() => Guid_tool[node_guid].Run(tool)));
                        }
                        if (list_task.Count > 0)
                        {
                            Result_list.Add(await Task.WhenAll(list_task));
                        }
                        sw1.Stop();
                        var sww1 = sw1.ElapsedMilliseconds;
                    }
                    if (Result_list.Count > 0)
                    {
                        Result_dic.Clear();
                        List<bool> is_Lasts = new List<bool>();
                        foreach (BaseTool[] basetool in Result_list)
                        {
                            for (int n = 0; n < basetool.Length; n++)
                            {
                                if (Result_dic.ContainsKey(basetool[n].STN_guid))
                                {
                                    Result_dic[basetool[n].STN_guid] = basetool[n];
                                }
                                else
                                {
                                    Result_dic.Add(basetool[n].STN_guid, basetool[n]);
                                }
                                if (all_Result_dic.ContainsKey(basetool[n].STN_guid))
                                {
                                    all_Result_dic[basetool[n].STN_guid] = basetool[n];
                                }
                                else {
                                    all_Result_dic.Add(basetool[n].STN_guid, basetool[n]);
                                }



                                if (basetool[n].OutputNode_guid.Count > 0)
                                {
                                    is_Lasts.Add(false);
                                }
                                else
                                {
                                    is_Lasts.Add(true);
                                }
                            }
                        }
                        var count = is_Lasts.Where(wh => wh == true).Count();
                        if (count == is_Lasts.Count)
                        {
                            is_last = true;
                        }
                        Result_list.Clear();
                    }
                }
                i++;
                if (i > 50)
                {
                    is_last = true;
                }
                Thread.Sleep(3);
            }
            allTool_time.Stop();
            var time = allTool_time.ElapsedMilliseconds;

            var dadsd = Result_dic.Values;
            //实例化一个ListViewItem
            ListViewItem listView = new ListViewItem();
            listView.Text = Num.ToString();
            foreach (BaseTool baseTool in Result_dic.Values)
            {
                if (baseTool.Result != null)
                {
                    listView.SubItems.Add(baseTool.Result.ToString());
                }
                else
                {
                    listView.SubItems.Add(baseTool.Score.ToString());
                }
                listView.SubItems.Add(baseTool.IsOK.ToString());
                listView.SubItems.Add(time.ToString() + "ms");
            }
            Num++;
            info_list.Items.Insert(0, listView);

        }

    }
}
