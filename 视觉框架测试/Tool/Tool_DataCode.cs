﻿using ChoiceTech.Halcon.Control;
using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Xml.Serialization;
using ViewWindow.Config;

namespace 视觉框架测试.Tool
{
    [Serializable]
    public class Tool_DataCode : BaseTool
    {
        public Tool_DataCode()
        {
            Tool_Type = typeof(Tool_DataCode);
            //Tool_guid = Guid.NewGuid();
            ini();
        }
        /// <summary>
        ///  识别数量
        /// </summary>
        /// 
        public int ResultMaxNum { get; set; }
        /// <summary>
        /// 二维码类型
        /// </summary>
        /// 
        public string CodeType { get; set; }
        public bool is_Follow { get; set; }

        /// <summary>
        /// 超时
        /// </summary>
        /// 
        public int TimerOut { get; set; }
        public string Def_Value { get; set; }
        public override void ini()
        {
            TimerOut = 100;
            CodeType = "QR Code";
            ResultMaxNum = 1;
            Search_Region = new Rectangle2();

        }
        public override BaseTool Run(BaseTool baseTool)
        {
            //记录程序运行时间
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BaseTool DeepCopy_Tool = DeepCopy(baseTool);
            if (Search_Region != null)
            {
                HOperatorSet.GenRectangle2(out HObject rect2, Search_Region.Row, Search_Region.Column, Search_Region.Phi, Search_Region.Lenth1, Search_Region.Lenth2);
                HOperatorSet.ReduceDomain(baseTool.Image, rect2, out HObject imageReduce);
                HOperatorSet.CropDomain(imageReduce,out HObject imagePart);
                //HOperatorSet.GetImageSize(baseTool.Image, out HTuple width, out HTuple height);
                //HOperatorSet.SetPart(Form1.setform1.hWindowControl1.HalconWindow, 0, 0, height, width);
                //Form1.setform1.hWindowControl1.HalconWindow.DispObj(imagePart);
                //Form1.setform1.hWindowControl1.HalconWindow.DispObj(rect2);
                HOperatorSet.CreateDataCode2dModel(CodeType, "default_parameters", "maximum_recognition", out HTuple DataCodeHandle);
                HOperatorSet.SetDataCode2dParam(DataCodeHandle, new HTuple("polarity"), new HTuple("any"));
                //HOperatorSet.SetDataCode2dParam(DataCodeHandle, "timeout", TimerOut);
                HOperatorSet.FindDataCode2d(imagePart, out HObject symbolXLDs, DataCodeHandle, "stop_after_result_num",
                ResultMaxNum, out HTuple ResultHandles, out HTuple decodedDataStrings);

                if (decodedDataStrings != null)
                {
                    if (decodedDataStrings.ToString() == "[]")
                    {
                        DeepCopy_Tool.IsOK = false;
                    }
                    else
                    {
                        if (decodedDataStrings.ToString() == Def_Value)
                        {
                            DeepCopy_Tool.IsOK = true;
                        }
                        else
                        {
                            DeepCopy_Tool.IsOK = false;
                        }
                    }

                    DeepCopy_Tool.Result = decodedDataStrings.ToString();
                    DeepCopy_Tool.SymbolXLDs = symbolXLDs;
                    DeepCopy_Tool.SymbolXLDs = SymbolXLDs;
                    //baseTool.Search_Region = 
                }
                HOperatorSet.ClearDataCode2dModel(DataCodeHandle);
            }
            sw.Stop();
            DeepCopy_Tool.Image = baseTool.Image;
            DeepCopy_Tool.Search_Region = Search_Region;
            DeepCopy_Tool.ConsumeTime = sw.ElapsedMilliseconds;
            DeepCopy_Tool.OutputNode_guid = OutputNode_guid;
            DeepCopy_Tool.STN_guid = STN_guid;
            return DeepCopy_Tool;
        }

    }
}
