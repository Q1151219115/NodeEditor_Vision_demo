﻿using HalconDotNet;
using ST.Library.UI.NodeEditor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 视觉框架测试.Tool;
using static 视觉框架测试.Form2;

namespace 视觉框架测试.STNodeModel
{
    [STNode("图像识别", "LQ", "2020/12/29", "V1.0", "Description")]
    public class STN_QRCode :STNode,IBaseNode
    {
        STNodeOption m_op_img_in, m_op_img_out;
        public Tool_DataCode tool_DataCode = new Tool_DataCode();


        protected override void OnCreate()
        {

            base.OnCreate();
            this.Title = "识别二维码";
            //此添加方式会得到添加成功后的 STNodeOption 索引位置
            m_op_img_in = this.InputOptions.Add("输入", typeof(BaseTool), true);
            m_op_img_in.DataTransfer += new STNodeOptionEventHandler(m_op_img_in_DataTransfer);
            m_op_img_in.Connected += M_op_img_in_Connected;
            m_op_img_in.DisConnected += M_op_img_in_DisConnected;
            m_op_img_out = this.OutputOptions.Add("输出", typeof(BaseTool), true);
            m_op_img_out.Connecting += M_op_img_out_Connecting;
            m_op_img_out.DisConnected += M_op_img_out_DisConnected;

            tool_DataCode.STN_guid = this.Guid;
            ToolType = typeof(Tool_DataCode);
        }

        private void M_op_img_out_DisConnected(object sender, STNodeOptionEventArgs e)
        {
            tool_DataCode.OutputNode_guid.Remove(e.TargetOption.Owner.Guid);
        }

        private void M_op_img_in_DisConnected(object sender, STNodeOptionEventArgs e)
        {
            tool_DataCode.InputNode_guid = Guid.Empty;
        }

        private void M_op_img_out_Connecting(object sender, STNodeOptionEventArgs e)
        {
            if (!tool_DataCode.OutputNode_guid.Contains(e.TargetOption.Owner.Guid))
            {
                tool_DataCode.OutputNode_guid.Add(e.TargetOption.Owner.Guid);
            }
        }

        private void M_op_img_in_Connected(object sender, STNodeOptionEventArgs e)
        {
            tool_DataCode.InputNode_guid = e.TargetOption.Owner.Guid;
        }

        private void m_op_img_in_DataTransfer(object sender, STNodeOptionEventArgs e)
        {
            //如果当前不是连接状态 或者 接受到的数据为空
            if (e.Status != ConnectionStatus.Connected || e.TargetOption.Data == null)
            {
                m_op_img_out.TransferData(null);    //向所有输出节点输出空数据
            }
            else
            {
                BaseTool basetool = e.TargetOption.Data as BaseTool;
                var obj = basetool.BaseValToDriVal(basetool, tool_DataCode);
                tool_DataCode = obj as Tool_DataCode;
                m_op_img_out.TransferData(basetool);
            }
        }
        public void Show_Set_Form()
        {
            HandledSetVal handledSet = new HandledSetVal(Delegate_SetVal);
            Form2.SingleShow(tool_DataCode, handledSet).Show();
        }

        public void Delegate_SetVal(Tool_DataCode tl_DataCode)
        {
            tool_DataCode = tl_DataCode;
            m_op_img_out.TransferData(tool_DataCode);
        }
        public BaseTool Get_Tool()
        {
            return tool_DataCode;
        }
        public void Set_Tool<T>(T _tool)
        {
            tool_DataCode = _tool as Tool_DataCode;
        }
    }
}
