﻿using ST.Library.UI.NodeEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 视觉框架测试.Tool;

namespace 视觉框架测试.STNodeModel
{
    [STNode("判断结果", "LQ", "2020/12/29", "V1.0", "Description")]
    public class STN_JudgeResult : STNode
    {
        STNodeOption m_op_img_in, m_op_img_out;
        public Tool_JudgeResult tool_judgeresult = new Tool_JudgeResult();
        protected override void OnCreate()
        {
            base.OnCreate();
            this.Title = "判断结果";
            //此添加方式会得到添加成功后的 STNodeOption 索引位置
            m_op_img_in = this.InputOptions.Add("输入", typeof(BaseTool), false);
            m_op_img_out = this.OutputOptions.Add("输出", typeof(BaseTool), false);
            m_op_img_in.DataTransfer += new STNodeOptionEventHandler(m_op_img_in_DataTransfer);
            m_op_img_in.Connected += M_op_img_in_Connected;
            m_op_img_in.DisConnected += M_op_img_in_DisConnected;
            m_op_img_out.Connecting += M_op_img_out_Connecting;
            m_op_img_out.DisConnected += M_op_img_out_DisConnected;
            tool_judgeresult.STN_guid = this.Guid;
            ToolType = typeof(Tool_JudgeResult);
        }

        private void M_op_img_out_DisConnected(object sender, STNodeOptionEventArgs e)
        {
            tool_judgeresult.OutputNode_guid.Remove(e.TargetOption.Owner.Guid);
        }

        private void M_op_img_out_Connecting(object sender, STNodeOptionEventArgs e)
        {
            if (!tool_judgeresult.OutputNode_guid.Contains(e.TargetOption.Owner.Guid))
            {
                tool_judgeresult.OutputNode_guid.Add(e.TargetOption.Owner.Guid);
            }
        }

        private void M_op_img_in_DisConnected(object sender, STNodeOptionEventArgs e)
        {
            tool_judgeresult.InputNode_guid = Guid.Empty;

        }

        private void M_op_img_in_Connected(object sender, STNodeOptionEventArgs e)
        {
            tool_judgeresult.InputNode_guid = e.TargetOption.Owner.Guid;
        }

        private void m_op_img_in_DataTransfer(object sender, STNodeOptionEventArgs e)
        {


        }
        public BaseTool Get_Tool()
        {
            return tool_judgeresult;
        }
    }
}
