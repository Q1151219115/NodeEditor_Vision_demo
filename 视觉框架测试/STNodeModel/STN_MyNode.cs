﻿using ST.Library.UI.NodeEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 视觉框架测试.STNodeModel
{
    [STNode("TEST", "LQ", "2020/12/29", "V1.0", "Description")]
    public class STN_MyNode : STNode
    {
/*        public MyNode()
        { //与OnCreate()等效
            this.Title = "TestNode";

        }*/
        protected override void OnCreate()
        {
            base.OnCreate();
            this.Title = "TestNode";
            //此添加方式会得到添加成功后的 STNodeOption 索引位置
            int nIndex = this.InputOptions.Add(new STNodeOption("IN_1", typeof(string), false));
            //此添加方式能直接得到一个构建的 STNodeOption
            STNodeOption op = this.InputOptions.Add("IN_2", typeof(int), true);

            this.OutputOptions.Add("OUT", typeof(string), true);
        }
    }

}
