﻿using HalconDotNet;
using ST.Library.UI.NodeEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 视觉框架测试.Tool;

namespace 视觉框架测试.STNodeModel
{
    [STNode("图像显示", "LQ", "2020/12/29", "V1.0", "Description")]
    public class STN_Display: STNode
    {
        private STNodeOption m_op_img_in;   //输入的节点
        Tool_Display tool_Display = new Tool_Display();
        [STNodeProperty("Name", "Description for this property")]

        protected override void OnCreate()
        {
            base.OnCreate();
            this.Title = "图像显示";
            //此添加方式会得到添加成功后的 STNodeOption 索引位置
            m_op_img_in = this.InputOptions.Add("IN_1", typeof(BaseTool), true);
            m_op_img_in.Connected += M_op_img_in_Connected;
            m_op_img_in.DisConnected += M_op_img_in_DisConnected;
            //当输入节点有数据输入时候
            m_op_img_in.DataTransfer += new STNodeOptionEventHandler(m_op_img_in_DataTransfer);
            tool_Display.HWin_Serial_No = 0;
            tool_Display.STN_guid = this.Guid;
            //m_op_img_out = this.OutputOptions.Add("OUT", typeof(string), true);
        }

        private void M_op_img_in_DisConnected(object sender, STNodeOptionEventArgs e)
        {
            tool_Display.InputNode_guid = Guid.Empty;
        }

        private void M_op_img_in_Connected(object sender, STNodeOptionEventArgs e)
        {
            tool_Display.InputNode_guid = e.TargetOption.Owner.Guid;
        }

        private void m_op_img_in_DataTransfer(object sender, STNodeOptionEventArgs e)
        {
            //如果当前不是连接状态 或者 接受到的数据为空
            if (e.Status != ConnectionStatus.Connected || e.TargetOption.Data == null)
            {
                //m_op_img_out.TransferData(null);    //向所有输出节点输出空数据
            }
            else
            {
                BaseTool basrtool = e.TargetOption.Data as BaseTool;
                if (basrtool.Image != null)
                {
                    HOperatorSet.GetImageSize(basrtool.Image, out HTuple width, out HTuple height);
                    HOperatorSet.SetPart(Form1.setform1.hWindowControl1.HalconWindow, 0, 0, height, width);
                    basrtool.Image.DispObj(Form1.setform1.hWindowControl1.HalconWindow);
                }
                if (basrtool.Search_Region != null)
                {
                    HOperatorSet.SetColor(Form1.setform1.hWindowControl1.HalconWindow, "green");
                    HOperatorSet.SetLineWidth(Form1.setform1.hWindowControl1.HalconWindow, 2);
                    HOperatorSet.SetDraw(Form1.setform1.hWindowControl1.HalconWindow, "margin");
                    //basrtool.Search_Region.DispObj(Form1.setform1.hWindowControl1.HalconWindow);

                }

            }
        }

        public BaseTool Get_Tool()
        {
            return tool_Display;
        }
    }
}
