# 基于流程编辑的视觉框架小Demo

#### 介绍
基于流程编辑的视觉框架小Demo
![image](https://gitee.com/liuqiang1995/NodeEditor_Vision_demo/raw/master/resources/20230106163205.png)
![image](https://gitee.com/liuqiang1995/NodeEditor_Vision_demo/raw/master/resources/20230106163323.png)


用到的其他开源代码：
1.HWindow_Tool： http://www.ihalcon.com/read-19264.html
2. STNode:  https://gitee.com/DebugST/DotNet_WinForm_NodeEditor



#### 使用说明

1.  双击流程节点可以打开编辑页面。 
2.  基于VS2022  .net4.8 ，需要将Debug下的halcondotnet.dll换成你自己电脑上装的。 
3.  NodeEditor_Vision_demo\视觉框架测试\HWindow_Tool\ihalcon开源窗体控件\控件demo\控件demo\bin\Debug  下的halcondotnet.dll也需要换成你自己电脑上的。然后生成项目，并将HWindow_Tool.dll放入主项目Debug下

